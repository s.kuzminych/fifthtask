﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Calculator.ConsoleUI
{
    class Calculator
    {
        static List<string> previousCalculations = new List<string>();
        static double? lastOperationResult;
        public void Operate()
        {
            while (true)
            {
                Console.WriteLine("Please, enter a command or expression. Type \"h\" to see all commands.");
                string input = Console.ReadLine();
                try
                {
                    switch (input)
                    {
                        case "c":
                        case "clear":
                            Clear();
                            break;
                        case "e":
                        case "exit":
                            ExitApp();
                            break;
                        case "h":
                        case "help":
                            ShowHelp();
                            break;
                        case "m":
                        case "memory":
                            ShowMemory();
                            break;
                        case "mc":
                        case "memory clear":
                            previousCalculations.Clear();
                            Console.WriteLine("Memory cleared.\n");
                            break;
                        case "mm":
                        case "multiply matrix":
                            lastOperationResult = null;
                            double[,] firstMatrix = CreateMatrix();
                            double[,] secondMatrix = CreateMatrix();
                            if (firstMatrix.GetLength(1) != secondMatrix.GetLength(0))
                            {
                                throw new InvalidOperationException("Incorrect expression. The number of columns on matrix A is not the same as the number of rows of matrix B. These matrices cannot be multiplied.");
                            }
                            firstMatrix = FillMatrix(firstMatrix);
                            secondMatrix = FillMatrix(secondMatrix);
                            MatrixMultiplication matrixMultiplication = new MatrixMultiplication(firstMatrix, secondMatrix);
                            double[,] resultMatrix = matrixMultiplication.Calculate();
                            string result = GetResultStringOfMatrixMultiplication(firstMatrix, secondMatrix, resultMatrix);
                            Console.WriteLine(result + "\n");
                            previousCalculations.Add(result);
                            break;
                        default:
                            ParseCommand(input.Trim(' '));
                            break;
                    }
                }
                catch (DivideByZeroException e)
                {
                    Console.WriteLine(e.Message);
                    lastOperationResult = null;
                }
                catch (InvalidOperationException e)
                {
                    Console.WriteLine(e.Message);
                }
                catch (ArgumentOutOfRangeException e)
                {
                    Console.WriteLine(e.Message);
                    lastOperationResult = null;
                }
                catch (ArgumentException e)
                {
                    Console.WriteLine(e.Message);
                }
            }
        }

        static void ShowHelp()
        {
            Console.WriteLine();
            Console.WriteLine("This application is a calculator with 4 basic arithmetic operations and matrix multiplication.");
            Console.WriteLine();
            Console.WriteLine("Arithmetic operations commands:");
            Console.WriteLine();
            Console.WriteLine("    Enter an expression \"operand operator operand\", in which the operator can be '+', '-', '*' or '/' (eg \"10 * 68\"). You can use your last operation result by entering command without first operand (eg \"- 10\"");
            Console.WriteLine("    Enter \"mm\" or \"matrix mp\" to do matrix multiplication.");
            Console.WriteLine();
            Console.WriteLine("Auxiliary operations:");
            Console.WriteLine();
            Console.WriteLine("    Enter \"c\" or \"clear\" to clear the calculation buffer. You can enter this command at any time.");
            Console.WriteLine("    Enter \"e\" or \"exit\" to exit the application. You can enter this command at any time.");
            Console.WriteLine("    Enter \"h\" or \"help\" to see this message again. You can enter this command at any time.");
            Console.WriteLine("    Enter \"m\" or \"memory\" to see the history of previous calculations.");
            Console.WriteLine("    Enter \"mc\" or \"memory clear\" to clear the history of previous calculations.");
            Console.WriteLine();
        }

        static void ShowMemory()
        {
            if (previousCalculations.Count < 1)
            {
                Console.WriteLine("There is no previous calculations.\n");
            }
            else
            {
                Console.WriteLine("Previous calculations:\n");
                foreach (var calculation in previousCalculations)
                {
                    Console.WriteLine(calculation);
                }
            }
        }

        static void Clear()
        {
            Console.WriteLine("The result of the previous operation is cleared.\n");
            lastOperationResult = null;
            Calculator calculator = new Calculator();
            calculator.Operate();
        }

        static void ExitApp()
        {
            Environment.Exit(0);
        }

        static void ParseCommand(string input)
        {
            char[] operators = { '+', '-', '*', '/' };
            List<string> elements = new List<string>(input.Split(' '));
            if (elements.Count == 3)
            {
                if (elements[1].Length != 1)
                {
                    throw new ArgumentException("Incorrect expression. Operator was too long.");
                }
                if (!operators.Contains(elements[1][0]))
                {
                    throw new ArgumentException("Incorrect expression. Missing operator.");
                }
                char mathOperator = elements[1][0];
                if (!double.TryParse(elements[0], out double firstOperand))
                {
                    throw new ArgumentException("Incorrect expression. Incorrect first operand.");
                }
                if (!double.TryParse(elements[2], out double secondOperand))
                {
                    throw new ArgumentException("Incorrect expression. Incorrect second operand.");
                }
                CallMathOperation(mathOperator, firstOperand, secondOperand);
            }
            else if (elements.Count == 2)
            {
                if (elements[0].Length != 1)
                {
                    throw new ArgumentException("Incorrect expression. Operator was too long.");
                }
                if (!operators.Contains(elements[0][0]))
                {
                    throw new ArgumentException("Incorrect expression. Missing operator.");
                }
                char mathOperator = elements[0][0];
                if (lastOperationResult == null)
                {
                    throw new ArgumentException("Incorrect expression. There is no last operation result.");
                }
                if (!double.TryParse(elements[1], out double secondOperand))
                {
                    throw new ArgumentException("Incorrect expression. Incorrect second operand.");
                }
                CallMathOperation(mathOperator, (double)lastOperationResult, secondOperand);
            }
            else
            {
                throw new ArgumentException("Incorrect expression. Too much arguments.");
            }

        }

        static void CallMathOperation(char mathOperator, double firstOperand, double secondOperand)
        {
            BaseOperation<double> operation;
            switch (mathOperator)
            {
                case '+':
                    operation = new Sum(firstOperand, secondOperand);
                    break;
                case '-':
                    operation = new Subtract(firstOperand, secondOperand);
                    break;
                case '*':
                    operation = new Multiplication(firstOperand, secondOperand);
                    break;
                case '/':
                    operation = new Division(firstOperand, secondOperand);
                    break;
                default:
                    return;
            }
            lastOperationResult = operation.Calculate();
            string expression = firstOperand.ToString() + ' ' + mathOperator + ' ' + secondOperand.ToString() + " = " + lastOperationResult.ToString();
            Console.WriteLine("Result is:\n" + expression + "\n");
            previousCalculations.Add(expression);
        }

        static double[,] CreateMatrix()
        {
            double[,] matrix = new double[GetMatrixParameter("Enter the number of rows of the matrix. It must be an integer in [1;9]"), GetMatrixParameter("Enter the number of columns of the matrix. It must be an integer in [1;9]")];
            return matrix;
        }

        static double[,] FillMatrix(double[,] matrix)
        {
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    matrix[i, j] = GetMatrixElement("Enter element [" + (i + 1).ToString() + "," + (j + 1).ToString() + "]:");
                }
            }
            return matrix;
        }

        static double GetMatrixElement(string message)
        {
            Console.WriteLine(message);
            string input = Console.ReadLine();
            double result = 0;
            switch (input)
            {
                case "h":
                case "help":
                    ShowHelp();
                    result = GetMatrixElement(message);
                    break;
                case "e":
                case "exit":
                    ExitApp();
                    break;
                case "c":
                case "clear":
                    Clear();
                    break;
                default:
                    if ((!double.TryParse(input, out result)) || (input.Contains(",")))
                    {
                        Console.WriteLine("This is not a valid number. Please, try again.");
                        result = GetMatrixElement(message);
                    }
                    break;
            }
            return result;
        }

        static byte GetMatrixParameter(string message)
        {
            Console.WriteLine(message);
            string input = Console.ReadLine();
            byte result = 0;
            switch (input)
            {
                case "h":
                case "help":
                    ShowHelp();
                    result = GetMatrixParameter(message);
                    break;
                case "e":
                case "exit":
                    ExitApp();
                    break;
                case "c":
                case "clear":
                    Clear();
                    break;
                default:
                    if ((!byte.TryParse(input, out result)) || (input.Contains(",")))
                    {
                        Console.WriteLine("This parameter is invalid. Please, try again.");
                        result = GetMatrixParameter(message);
                    }
                    else if ((result < 0) || (result > 9))
                    {
                        Console.WriteLine("This parameter must be in [1;9]. Please, try again.");
                        result = GetMatrixParameter(message);
                    }
                    break;
            }
            return result;

        }

        static string GetResultStringOfMatrixMultiplication(double[,] firstMatrix, double[,] secondMatrix, double[,] resultMatrix)
        {
            string resultString = "";
            int firstMatrixColCount = firstMatrix.GetLength(1);
            int firstMatrixRowCount = firstMatrix.GetLength(0);
            int secondMatrixColCount = secondMatrix.GetLength(1);
            int secondMatrixRowCount = secondMatrix.GetLength(0);
            int resultMatrixColCount = resultMatrix.GetLength(1);
            int resultMatrixRowCount = resultMatrix.GetLength(0);
            int maxRowCount = secondMatrixRowCount;
            if (firstMatrixRowCount > secondMatrixRowCount)
            {
                maxRowCount = firstMatrixRowCount;
            }
            int middleRow = maxRowCount / 2;
            Console.WriteLine("Result is:");
            resultString += " __" + new string(' ', ((5 * firstMatrixColCount) - 2)) + "__     __" + new string(' ', ((5 * secondMatrixColCount) - 2)) + "__     __" + new string(' ', ((5 * resultMatrixColCount) - 2)) + "__\n";
            resultString += "|" + new string(' ', (5 * firstMatrixColCount)) + "  |   |" + new string(' ', (5 * secondMatrixColCount) - 1) + "   |   |" + new string(' ', (5 * resultMatrixColCount) - 1) + "   |\n";
            for (int i = 0; i < maxRowCount; i++)
            {
                resultString += "|";
                for (int j = 0; j < firstMatrixColCount; j++)
                {
                    if (i >= firstMatrixRowCount)
                    {
                        resultString += "     ";
                    }
                    else
                    {
                        resultString += string.Format("{0,5}", firstMatrix[i, j].ToString());
                    }
                }
                resultString += "  |";
                if (middleRow == i)
                {
                    resultString += " x |";
                }
                else
                {
                    resultString += "   |";
                }

                for (int j = 0; j < secondMatrixColCount; j++)
                {
                    if (i >= secondMatrixRowCount)
                    {
                        resultString += "     ";
                    }
                    else
                    {
                        resultString += string.Format("{0,5}", secondMatrix[i, j].ToString());
                    }
                }
                resultString += "  |";
                if (middleRow == i)
                {
                    resultString += " = |";
                }
                else
                {
                    resultString += "   |";
                }
                for (int j = 0; j < resultMatrixColCount; j++)
                {
                    if (i >= resultMatrixRowCount)
                    {
                        resultString += "     ";
                    }
                    else
                    {
                        resultString += string.Format("{0,5}", resultMatrix[i, j].ToString());
                    }
                }
                resultString += "  |\n";
            }
            resultString += "|__" + new string(' ', ((5 * firstMatrixColCount) - 2)) + "__|   |__" + new string(' ', ((5 * secondMatrixColCount) - 2)) + "__|   |__" + new string(' ', ((5 * resultMatrixColCount) - 2)) + "__|";
            return resultString;
        }
    }
}
