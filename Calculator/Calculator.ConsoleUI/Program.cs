﻿namespace Calculator.ConsoleUI
{
    class Program
    {
        static void Main(string[] args)
        {
            Calculator calculator = new Calculator();
            calculator.Operate();
        }
    }
}
