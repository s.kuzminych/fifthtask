﻿using System;

namespace Calculator
{
    public abstract class BaseOperation<T>
    {
        public T FirstOperand;
        public T SecondOperand;
        public BaseOperation(T firstOperand, T secondOperand)
        {
            FirstOperand = firstOperand;
            SecondOperand = secondOperand;
        }
        abstract public T Calculate();
    }
    public class Sum : BaseOperation<double>
    {
        public Sum(double firstOperand, double secondOperand) : base(firstOperand, secondOperand) { }
        public override double Calculate()
        {
            return FirstOperand + SecondOperand;
        }
    }
    public class Subtract : BaseOperation<double>
    {
        public Subtract(double firstOperand, double secondOperand) : base(firstOperand, secondOperand) { }
        public override double Calculate()
        {
            return FirstOperand - SecondOperand;
        }
    }
    public class Multiplication : BaseOperation<double>
    {
        public Multiplication(double firstOperand, double secondOperand) : base(firstOperand, secondOperand) { }
        public override double Calculate()
        {
            if ((FirstOperand * SecondOperand > double.MaxValue) || (FirstOperand * SecondOperand < double.MinValue))
            {
                throw new ArgumentOutOfRangeException("Incorrect expression. Result is too big.");
            }
            return FirstOperand * SecondOperand;
        }
    }
    public class Division : BaseOperation<double>
    {
        public Division(double firstOperand, double secondOperand) : base(firstOperand, secondOperand) { }
        public override double Calculate()
        {
            if (SecondOperand == 0)
            {
                throw new DivideByZeroException("Incorrect expression. Number can not be divided by zero.");
            }
            if ((FirstOperand / SecondOperand > double.MaxValue) || (FirstOperand / SecondOperand < double.MinValue))
            {
                throw new ArgumentOutOfRangeException("Incorrect expression. Result is too small.");
            }
            return FirstOperand / SecondOperand;
        }
    }
    public class MatrixMultiplication : BaseOperation<double[,]>
    {
        public MatrixMultiplication(double[,] firstOperand, double[,] secondOperand) : base(firstOperand, secondOperand) { }
        public override double[,] Calculate()
        {
            double[,] resultMatrix = new double[FirstOperand.GetLength(0), SecondOperand.GetLength(1)];
            for (int i = 0; i < FirstOperand.GetLength(0); i++)
            {
                for (int j = 0; j < SecondOperand.GetLength(1); j++)
                {
                    for (int k = 0; k < FirstOperand.GetLength(1); k++)
                    {
                        resultMatrix[i, j] = resultMatrix[i, j] + FirstOperand[i, k] * SecondOperand[k, j];
                    }
                }
            }
            return resultMatrix;
        }
    }
}
